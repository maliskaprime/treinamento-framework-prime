﻿namespace FAPC.PageObjects.Desktop
{
    public class CalcPage : TestBase
    {
        private new const string Title = "Calculadora";

        public static FwControl ClickNum4(int timeout = DefaultTimedOut)
        {
            FwControl control = new FwControl(Title);

            control.ClassNameNn = "[CLASS:Button; INSTANCE:4]";

            control.Click(timeout);

            return control;
        }

        public static FwControl ClickIgual(int timeout = DefaultTimedOut)
        {
            FwControl control = new FwControl(Title);

            control.ClassNameNn = "[CLASS:Button; INSTANCE:28]";

            control.Click(timeout);

            return control;
        }

        public static FwControl ClickSubtrair(int timeout = DefaultTimedOut)
        {
            FwControl control = new FwControl(Title);

            control.ClassNameNn = "Button22";

            control.Click(timeout);

            return control;
        }

        public static FwControl ClickAdicionar(int timeout = DefaultTimedOut)
        {
            FwControl control = new FwControl(Title);

            control.ClassNameNn = "Button23";

            control.Click(timeout);

            return control;
        }

        public static FwControl ClickNum2(int timeout = DefaultTimedOut)
        {
            FwControl control = new FwControl(Title);

            control.ClassNameNn = "Button11";

            control.Click(timeout);

            return control;
        }
    }
}